"""Breadth first search visits each layer of the tree starting fromt the top and iterating to the lowest layer."""

#  Method 1: Get the nodes of each level by getting the height of a given node
class Node: 
    def __init__(self, key):
        self.data = key
        self.left = None
        self.right = None

#  def height(node): 
    #  if node is None: 
        #  return 0
    #  else: 
        #  left_height = height(node.left)
        #  right_height = height(node.right) 

    #  return max(left_height, right_height) + 1 

#  def printBFSOrder(root): 
    #  h = height(root)
    #  for i in range(1, h + 1): 
        #  printLevelNodes(root, i)

#  def printLevelNodes(root, level): 
    #  if root is None: 
        #  return 
    #  if level ==1: 
        #  print(root.data)
    #  elif level > 1:
        #  printLevelNodes(root.left, level - 1)
        #  printLevelNodes(root.right, level - 1)


# Method2: Using a queue

def printLevelTraversal(root): 
    if root is None: 
        return 

    result = []
    nodes = [root]

    while nodes: 
        result.append([ n.data for n in nodes ])
        next_level_nodes = []
        
        for node in nodes: 
            if node.left: 
                next_level_nodes.append(node.left)

            if node.right: 
                next_level_nodes.append(node.right)

        nodes = next_level_nodes
    return result

# Driver program to test above function 
root = Node(1) 
root.left = Node(2) 
root.right = Node(3) 
root.left.left = Node(4) 
root.left.right = Node(5) 
  
print(printLevelTraversal(root))

