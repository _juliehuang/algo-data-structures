"""
Doubly linked lists allows for forwards and backwards traversal. 
Contains an extra pointer -  more efficient for deletion because you can access the previous node using the prev pointer. 
Disadvantage - must maintain two pointers
"""

class Node: 
    def __init__(self, data): 
        self.data = data
        self.next = None
        self.prev = None

class doubly_linked_list: 
    def __init__(self): 
        self.head= None

    def insertNode(self, new_data):
        new_node = Node(new_data)
        new_node.next = self.head
        if self.head: 
            self.head.prev = new_node
        self.head = new_node

    def print_list(self, node): 
        list_of_elements =[]
        while node: 
            list_of_elements.append(node.data)
            prev = node
            node = node.next
        print(list_of_elements)

# ------------------ Insertion After Node  -----------------
    # Time Complexity: O(1) - constant amount of work, only changing pointers
    # STEPS
        # - switch previous node's next pointer to point to new node,  
        # - new node's prev pointer points to prev_node
        # - new node's prev pointer points to prev_node
    def insertAfter(self, prev_node, new_data): 
        new_node = Node(new_data)
        prev_node.next = new_node 
        new_node.prev = prev_node
        new_node.next = prev_node.next

# ------------------ Deletion After Node  -----------------
    # Time Complexity: O(1) - constant amount of work, only changing pointers
    # STEPS
        # - target's previous node will not point to target's next  
        # - target's next node's previous pointer now points to target's previous
    def deleteNode(self, target_node): 
        target_node.prev = target_node.next 
        target_node.next = target_node.prev


if __name__ == '__main__': 
    dllist = doubly_linked_list()
    dllist.insertNode(1)
    dllist.insertNode(2)
    dllist.insertNode(3)
    dllist.print_list(dllist.head)
    newNode = Node(1)
    dllist.insertAfter(newNode, 7)
    dllist.print_list(dllist.head)
