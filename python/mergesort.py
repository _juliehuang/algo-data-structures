"""
Merge sort is another divide and conquer algorithm. Idea is you split an array into fairly equal parts, and keep
splitting until you have arrays of 1 element. Then merge those one element subarrays to form two element subarrays while
sorting. """

# Time Complexity: O(nlog(n)) since it divides the array into two and then takes n time to sort each array
def mergeSort(arr): 
    if len(arr) > 1: 
        mid = len(arr) // 2 
        left = arr[:mid]
        right = arr[mid:]

        mergeSort(left)
        mergeSort(right)

        # i is the left copy index
        # j is the right copy index
        # k is the arr array index
        i = j = k = 0 

        while i < len(left) and j < len(right): 
            if left[i] < right[j]: 
                arr[k] = left[i]
                i += 1
            
            else: 
                arr[k] = right[j]
                j += 1

            k += 1

        # check that there are any left over elements, (due to uneven partitions)
        while i < len(left): 
            arr[k] = left[i]
            i += 1
            k += 1

        while j < len(right): 
            arr[k] = right[j]
            j += 1
            k += 1
    return arr

array = [12, 11, 13, 5, 6, 7]
mergeSort(array)
print(array)
