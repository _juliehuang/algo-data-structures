"""
Bubble sort is typically the worst-performing sorting algorithms except when checking if an array is sorted. The idea is
to check adjacent paris and swap their positions if the first element is larger than the second. 
Add a flag so that if the list is already sorted it won't continue checking the elements in the array"""

def bubbleSort(arr): 
    has_swapped = True  # Note: when no swaps are made, the list is sorted

    while has_swapped: 
        has_swapped = False
        for c in range(len(arr) -1):         # iterate up to the last element
            if  arr[c] > arr[c+1]: 
                arr[c], arr[c+1]  = arr[c+1], arr[c]
                has_swapped = True
    return arr


our_list = [19, 13, 6, 2, 18, 8]
bubbleSort(our_list)
print(our_list)

            
