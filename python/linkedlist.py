"""
Linked lists are linear data strctures where elements are not stored in continuous memory locations.
Dynamic sizing compared to arrays which means easier to insert elements is less expensive (i.e, don't need to recreate
room and shift elements around). 
Elements in the linkied list are linked using nodes which consist of a data field and a reference(link) to the next
node.

    list.head       second              third
         |                |                  |
         |                |                  |
    +----+------+     +----+------+     +----+------+
    | 1  |  o-------->| 2  |  o-------->|  3 | null |
    +----+------+     +----+------+     +----+------+
"""

# define a node 
class Node: 
    def __init__(self, data): 
        self.data = data
        self.next = None

    def __repr__(self): 
        return f"Node({self.data})"

class LinkedList: 
    def __init__(self): 
        self.head = None                                # head is the first node in LL; initially None

    def print_list(self): 
        list_of_elements = []
        temp = self.head
        while temp: 
            list_of_elements.append(temp.data)
            temp = temp.next
        print(list_of_elements)


# ------------------ Insertion at Head of Linked List -----------------
    # Time Complexity: O(1) - constant amount of work 
    # STEPS
        # - switch head pointer to the new node
        # - point the new node to former head

    def insertHead(self, new_data): 

        # initialize new Node with new_data
        new_node = Node(new_data)
        if self.head: 
            new_node.next = self.head
        self.head = new_node

# ----------------- Insertion at End of Linked List --------------------------
    # Time Complexity: O(n) since you have to traverse through entire list to find the tail
    # STEPS
        # - traverse to the last Node (i.e., the node that has next pointer pointing to None
        # - switch the next pointer for the last node to the new node

    def insertTail(self, new_data): 
        new_node = Node(new_data)
        temp = self.head
        while temp.next is not None: 
            temp = temp.next
        temp.next = new_node

# ----------------- Insertion after given data of Linked List --------------------------
    # Time Complexity: O(n) as it has to traverse through the list looking to find the match with given_data 
    # STEPS
        # - traverse to the node with data =  given_data         
        # - switch the next pointer for that node to point to new_node
        # - new node has to point to the given node's next

    def insertAfter(self, given_data, new_data): 
        temp = self.head
        while temp.next is not None: 
            if temp.data == given_data: 
                break 
            temp = temp.next
        if temp is None: 
            print("item does not exist in list")
        else: 
            new_node = Node(new_data)
            new_node.next = temp.next
            temp.next = new_node

# ----------------- Delete First Occurance of given key in LL  --------------------------
    # Time Complexity: O(n) as it has to traverse through the list looking to find the node mat
    # STEPS
        # - traverse to the node with data = key to be deleted        
        # - switch the prev Node's  pointer to point to the target's next (i.e., leap-frogging)

    def deleteNode(self, to_be_deleted): 
        if self.head is None: 
            return 
        current_node = self.head
        previous_node = None

        while current_node.data != to_be_deleted: 
            previous_node = current_node
            current_node = current_node.next
            
            if current_node is None: 
                print("Data to be deleted does not exist in the list")
                return

        if previous_node is None:                            # if the node to be deleted is the head
            self.head = current_node.next
        else: 
            previous_node.next = current_node.next          # previous node points to the next not of the node to be del
            
# ----------------- Traverse a Linked List   --------------------------
    # Time Complexity: O(n) as it has to traverse through the entire list 
    # STEPS
        # - take the head, assign to variable,
        # - while next is not none, print each variable
        # - add case when there are no nodes

    def traverseList(self): 
        current_node = self.head

        if current_node is None: 
            print("Empty linked list")
            return

        while current_node is not None: 
            print(current_node)
            current_node = current_node.next

# ----------------- Reverse  a Linked List   --------------------------
    # Time Complexity: O(n) as it has to traverse through the entire list 
    # STEPS
        # - head now points to null 
        # - current now points to prev

    def reverseList(self): 
        previous_node = None
        current_node = self.head

        while current_node: 
            # keep track of the next_node
            next_node = current_node.next 
            # point the current node's next backwards
            current_node.next = previous_node
            # make the previous node current node
            previous_node = current_node 
            # make the current node the next node 
            current_node = next_node

        self.head = previous_node


if __name__ == '__main__': 
    test_ll = LinkedList()
    test_ll.insertHead(1)
    #  print(test_ll.head)
    test_ll.insertHead(2)
    test_ll.insertHead(3)
    #  test_ll.print_list()
    #  test_ll.insertTail(4)
    #  test_ll.print_list()
    #  test_ll.insertAfter(2, 7)
    #  test_ll.print_list()
    #  test_ll.deleteNode(3)
    #  test_ll.deleteNode(10)
    #  test_ll.print_list()
    #  test_ll.traverseList()
    test_ll.reverseList()
    test_ll.print_list()


