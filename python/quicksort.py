"""Quicksort is a divide and conquer algorithm. It picks a pivot element and partitions the array around the picked pivot.
Quicksort works in place meaning that it doesn't create any copies of the array to be sorted.  Quicksort out performs merge sort. Pick a pivot, then everything right of it must be larger and
everything less than it should be on the left of it. Recursively sort those sub-arrays (left and right arrays of the
pivot) 
Source: https://stackabuse.com/quicksort-in-python/

Time Complexity: O(n^2) if partions are unbalanced"""

# start is the pivot
# end is the index of end of the array/subarray
def partition(arr, start, end): 
    pivot = arr[start]
    low = start + 1
    high = end

    while True: 
        # keep moving high left until you find element that's smaller than pivot 
        # make sure we haven't surpassed lower pointer 'cause that means we all elements are in the correct positon
        while low <= high and arr[high] >= pivot: 
            high = high - 1 
        # move low right until you find element that's larger than pivot
        while low <= high and arr[low] <= pivot: 
            low = low + 1
        
        if low <= high: 
            arr[low], arr[high] = arr[high], arr[low]
        else: 
            break

    # once low/high indices are the same, swap it with the pivot
    arr[start], arr[high] = arr[high], arr[start]

    return high 
    
def quickSort(arr, start, end): 
    if start >= end:
        return 
    p = partition(arr, start, end)
    quickSort(array, start, p-1)
    quickSort(array, p+1, end)


array = [29,99,27,41,66,28,44,78,87,19,31,76,58,88,83,97,12,21,44]

quickSort(array, 0, len(array) - 1)
print(array)

