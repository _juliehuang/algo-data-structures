"""
Linear data structures similar to stack that follows FIFO. Basic operations include enqueue (add an item to the queue),
and dequeue (remove an item from the queue) """

class Queue: 
    def __init__(self): 
        self.items = []
        self.length = 0 
        self.front = None

    def enqueue(self, new_item): 
        if self.front is None: 
            self.front = new_item
            self.items.append(new_item)
        else: 
            self.items.append(new_item)

    def dequeue(self): 
        if self.front is None: 
           print("queue has no items") 
        else: 
            self.items.pop(0)

if __name__ == '__main__': 
    queue = Queue()
    a = [1,2,3,4,5,6]
    for i in a: 
        queue.enqueue(i)
    print(queue.items)
    queue.dequeue()
    print(queue.items)
    

