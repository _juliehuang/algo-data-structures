class Node: 
    def __init__(self, key): 
        self.left = None
        self.right = None
        self.val = key 
  
# pre-order traversal 
# root -> left -> right
# used to copy the tree
def preOrderTraversal(root): 
    if root is None: 
        return 

    print(root.val)

    preOrderTraversal(root.left)
    preOrderTraversal(root.right)


# left -> root -> right
# for binary search trees, this gives the tree in ascending order
def inOrderTraversal(root): 
    if root is None: 
        return 
    inOrderTraversal(root.left)
    print(root.val)
    inOrderTraversal(root.right)

# left -> right -> root
# used to delete a tree
def postOrderTraversal(root): 
    if root is None: 
        return 

    postOrderTraversal(root.left)
    postOrderTraversal(root.right)
    print(root.val)


# Driver code
root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
preOrderTraversal(root)
#  postOrderTraversal(root)
#  inOrderTraversal(root)
