"""
Stacks are linear data structure that follows LIFO. With two principal operations, push() and pop(). Essentially
elements can only be added and removed from one end. 
"""

class Stack(): 
    def __init__(self): 
        self.items = []

    # Time complexity: O(1) for push and pop since we're only operating on the one end of the list
    def push(self, new_item): 
        return self.items.append(new_item)

    def pop(self): 
        if not self.items: 
            print("no items in stack")
            return 
        else: 
            return self.items.pop()

    # see the most recent item of the stack
    def peek(self): 
        if not self.items: 
            print("no items in stack")
            return 
        return self.items[-1]
    
    def isEmpty(self): 
        if not self.items: 
            return True
    
    def sizeStack(self): 
        counter = 0
        for i in self.items: 
            counter += 1
        return counter
          
        


if __name__ == '__main__': 
    stack = Stack()
    stack.push(1)
    stack.push(2)
    stack.push(3)
    #  print(stack.items)
    #  print(stack.peek())
    stack.pop()
    print(stack.items)
    print(stack.isEmpty())
    print(stack.sizeStack())


