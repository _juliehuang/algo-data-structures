"""
Heaps are tree based data structures in which the tree is a complete binary tree. Generally there are two types: 
    Max Heap - the root node must be larger or equal to  all the nodes of tree and same property applies to the subtrees
    Min Heap - the root node must be the minimum or equal to all the nodes of the tree  and same property applies to the subtrees"""


#Given an array of numbers build a max heap

# Approach: 
# Leaf nodes don't need to be heapified b/c they already follow heap property
# Idea is to find the position of the last non-leaf node and perform heapify on each non-leaf node in reverse level
# order. Array representation is already in level order traversal of tree. The last non-leaf node = parent of the last
# node or the last non-leaf node = parent of the node at (n-1) index = node at index ((n-1) -1) /2 = n/2 -1 
# root is at index 0, left child of the ith node is (2*i + 1)th index, right child of th eith node is (2*1 + 2)th index,
# parent of the ith node is (i-1)/2 

# To heapify a subtree rooted with node i  
# which is an index in arr[]. N is size of heap 
def heapify(arr, n, i): 
    largest = i 
    left = 2*i + 1
    right = 2*i + 2

    if (left < n and arr[left] > arr[largest]):       # left child is larger than root of subtree
        largest = left 

    if (right < n and arr[right] > arr[largest]): 
        largest = right   # right child is larger than root of subtree

    if largest != i: 
        to_be_swapped = arr[i]
        arr[i] = arr[largest]
        arr[largest] = to_be_swapped

        heapify(arr, n, largest)

def buildHeap(arr, n): 
    startInd = int(n/2) - 1

    for i in range(startInd, -1, -1): 
        #  print(i)
        heapify(arr, n, i)
    return arr

def printHeap(arr, n): 
    for i in range(n): 
        print(arr[i])

if __name__ == '__main__': 
    arr = [ 1, 3, 5, 4, 6, 13,  
             10, 9, 8, 15, 17 ]

    new_heap = buildHeap(arr, len(arr))
    assert(new_heap == [17, 15, 13, 9, 6, 5, 10, 4, 8, 3, 1])
    printHeap(new_heap, len(arr))
 


