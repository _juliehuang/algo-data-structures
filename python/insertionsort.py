"""
Insertion sort is a stable (meaning same elements are sorted in the same order they were in unsorted array) and
in-place (does not use any auxilliary data structure)
Idea: An array is partitioned into a "sorted" subarray and an "unsorted" subarray. The sorted array only contains the
first element of the original array. The elements in the unsorted array are evaluated so we can insert it in the sorted
array. Insertion is done by moving all elements larger than the new element one position to the right.
Time Complexity: O(n^2) when the list is in decreasing order. 
Space Complexity: O(1) 
"""

def insertionSort(arr): 
    for i in range(1, len(arr)): 
        current_item = arr[i]
        current_pos = i

        # if the current item is smaller than the first element of sorted array, move the sorted array one over to the
        # right 
        while current_pos > 0 and arr[current_pos - 1] > current_item: 
            arr[current_pos] = arr[current_pos -1 ]
            current_pos = current_pos - 1  # do the same for the elements before


        # if we reach the beginning of the array or the new element is larger than the current_position's value, then
        # leave it in the same position
        arr[current_pos] = current_item 
    return arr

array = [4, 22, 41, 40, 27, 30, 36, 16, 42, 37, 14, 39, 3, 6, 34, 9, 21, 2, 29, 47]
insertionSort(array)
print("Sorted array: " + str(array))

        

