# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def hasPathSum(self, root: TreeNode, sum: int) -> bool: 
        if root is None: 
            return False 
        
        if root.val == sum: 
            if root.left is None and root.right is None: 
                return True
            else: 
                return self.hasPathSum(root.left, 0) or self.hasPathSum(root.right, 0)
        else: 
            return self.hasPathSum(root.left, sum - root.val) or self.hasPathSum(root.right, sum - root.val)
            
        
        
