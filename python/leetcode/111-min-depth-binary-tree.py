# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def minDepth(self, root: TreeNode) -> int:
        if root is None: 
            return 0 
        
        # only right exists
        if not root.left: 
            return 1 + self.minDepth(root.right)
        
        # only left exists
        elif not root.right: 
            return 1 + self.minDepth(root.left)
        
        # you reach a leaf child
        else: 
            return 1 + min(self.minDepth(root.left), self.minDepth(root.right))
            
