class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        # iterate through the list twice and check if the pairs of elements add up to target. If it does return those indices
#         for i in range(len(nums) -1):
#             for j in range(i+1, len(nums)): 
#                 if nums[i] + nums[j] == target: 
#                     return [i, j]
        diffs = {}
        
        for i, num in enumerate(nums): 
            complement = target - num # this will be the difference
            
            if complement not in diffs: # add it if this diff isn't already in the map
                diffs[num] = i 
            else: 
                return [diffs[complement], i]
                
        
