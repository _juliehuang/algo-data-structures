"""
Selection sorts an array by finding the minimum value of the unsorted part and then swapping it with the first unsorted
element. It is an in-place algorithm. Idea is to break the array into two parts, sorted part (which is empty to begin
with) and the unsorted part (the original array to begin with). Find the min value of unsorted part and swap it with the
first unsorted value and then increase the sorted part by 1. 
Time Complexity: O(n^2)"""

def selectionSort(arr): 
    for i in range(0, len(arr) -1): 
        min_ind = i 

        for j in range(i+1, len(arr) - 1): 
            if arr[j] < arr[min_ind]: 
                min_ind = j

        arr[i], arr[min_ind] = arr[min_ind], arr[i]

L = [3, 1, 41, 59, 26, 53, 59]
selectionSort(L)
print(L)
        

