"""
Non linear data structure composed of nodes which links up to two other child notes. Data of all the nodes in the left sub tree of root is less
than the data of the root. The data of all the nodes on the right are all greater than the data of root. 
"""


class Node:
    def __init__(self, data):
        self.data = data 
        self.left = None
        self.right = None

class BinarySearchTree: 
    def __init__(self): 
        self.root = None

    def insert(self, data): 
        if not self.root: 
            self.root = Node(data)
        else: 
            self.insertNode(data, self.root)

    def insertNode(self, data, root_node): 
        if data < root_node.data: 
            if root_node.left: 
                self.insertNode(data, root_node.left)
            else: 
                root_node.left = Node(data)
        else: 
            if root_node.right: 
                self.insertNode(data, root_node.right)
            else: 
                root_node.right = Node(data)

# ---------------- Traversals -------------------

    def traversal(self): 
        if self.root: 
            self.traversePostOrder(self.root)

    # Left -> Root -> Right
    def traverseInOrder(self, root_node): 
        if root_node.left: 
            self.traverseInOrder(root_node.left)
        print(root_node.data)
        if root_node.right: 
            self.traverseInOrder(root_node.right)

    # Root -> Left -> Right 
    def traversePreOrder(self, root_node):
        if root_node: 
            print(root_node.data)
            if root_node.left: 
                self.traversePreOrder(root_node.left)
            if root_node.right: 
                self.traversePreOrder(root_node.right)

    # Left -> Right -> Root
    def traversePostOrder(self, root_node): 
        if root_node.left: 
            self.traversePostOrder(root_node.left)
        if root_node.right: 
            self.traversePostOrder(root_node.right)
        print(root_node.data)


    def breadthFirstTraversal(self, root_node): 
        if root_node is None: 
            return None
        else: 
            # need function to get the height of the tree, and then loop over that for each level 
            h = height(root_node)
            for level in range(1, h+1): 
                if level ==1: 
                    print(root.data)
    
    def getHeight(self): 
        if self.root: 
            return self.height(self.root)

    def height(self, root_node): 
        if root_node is None: 
            return 0 
        else: 
            left_height = self.height(root_node.left)
            right_height = self.height(root_node.right)
        return max(left_height, right_height) + 1       # need to add one to account for the root node 

    def lookupVal(self, lookup_val): 
        if self.root: 
            return self.search(self.root, lookup_val)
        
    def search(self, root_node, lookup_val):
        if root_node.data ==lookup_val: 
            return root_node.data
        elif root_node.data < lookup_val: 
            return self.search(root_node.right, lookup_val)
        else: 
            return self.search(root_node.left, lookup_val)


if __name__ == '__main__':
    bst = BinarySearchTree()
    bst.insert(7) # ROOT node
    bst.insert(8)
    bst.insert(1)
    bst.insert(4)
    #  bst.traversal()
    print(bst.getHeight())
    #  print(bst.lookupVal(7))
    #  print(bst.lookupVal(10))
    
    #  print(bst.search(Node(7), 4))
    #  print(bst.height(Node(7)))
    #  print(bst.depthOfTree())
